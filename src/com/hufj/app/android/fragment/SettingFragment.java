package com.hufj.app.android.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.Toast;

import com.hufj.app.android.R;
import com.hufj.app.android.util.AppConstant;

public class SettingFragment extends Fragment {

	RadioButton front, rear, cAuto, cManual;
	EditText emergengyNo;
	SharedPreferences sharedpreferences;
	Button buttonAdd, buttonRemove;

	PopupWindow popWindow;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedpreferences = getActivity().getSharedPreferences(
				AppConstant.APP_HFUJ, Context.MODE_PRIVATE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.fragment_layout_setting, null);

		front = (RadioButton) rootView.findViewById(R.id.radio0);
		rear = (RadioButton) rootView.findViewById(R.id.radio1);
		cAuto = (RadioButton) rootView.findViewById(R.id.radioCameraAuto);
		cManual = (RadioButton) rootView.findViewById(R.id.radioCameraManual);
		emergengyNo = (EditText) rootView
				.findViewById(R.id.id_edit_emergency_no);
		buttonAdd = (Button) rootView.findViewById(R.id.id_button_contact);
		buttonRemove = (Button) rootView.findViewById(R.id.id_button_remove);
		setupRadio();
		setUpEmergencyNumber();

		addRadioClicklistener();
		addContactButtonClickListener();
		return rootView;
	}

	private void setUpEmergencyNumber() {
		// TODO Auto-generated method stub
		if (sharedpreferences.contains(AppConstant.EMERGENCY_NO)) {
			String number = sharedpreferences.getString(
					AppConstant.EMERGENCY_NO, "");
			if (number != "")
				emergengyNo.setText(number);
		}

	}

	private void addContactButtonClickListener() {
		buttonAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!emergengyNo.getText().toString().trim().equals("")) {
					showPopWindow();
				} else {

					Toast.makeText(getActivity(),
							"Please enter emergency contact number",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		buttonRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!emergengyNo.getText().toString().trim().equals("")) {
					Editor editor = sharedpreferences.edit();
					editor.remove(AppConstant.EMERGENCY_NO);
					editor.commit();
					emergengyNo.setText("");
					Toast.makeText(getActivity(), "Number Remove Successfully",
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getActivity(), "Empty Field",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void addRadioClicklistener() {

		rear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_ORIENTATION,
						AppConstant.CAMERA_REAR);
				editor.commit();
			}
		});
		front.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_ORIENTATION,
						AppConstant.CAMERA_FRONT);
				editor.commit();
			}
		});
		cAuto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_MODE,
						AppConstant.CAMERA_AUTO);
				editor.commit();
			}
		});
		cManual.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_MODE,
						AppConstant.CAMERA_MANUAL);
				editor.commit();
			}
		});

	}

	private void setupRadio() {
		if (sharedpreferences != null
				&& sharedpreferences.contains(AppConstant.CAMERA_ORIENTATION)) {
			String cOrientation = sharedpreferences.getString(
					AppConstant.CAMERA_ORIENTATION, AppConstant.CAMERA_REAR);

			if (cOrientation.equals(AppConstant.CAMERA_FRONT)) {
				front.setChecked(true);
			} else {
				rear.setChecked(true);

			}

		}
		if (sharedpreferences != null
				&& sharedpreferences.contains(AppConstant.CAMERA_MODE)) {
			String cMode = sharedpreferences.getString(AppConstant.CAMERA_MODE,
					AppConstant.CAMERA_AUTO);

			if (cMode.equals(AppConstant.CAMERA_MANUAL)) {
				cManual.setChecked(true);
			} else {
				cAuto.setChecked(true);
			}

		}
	}

	private void showPopWindow() {

		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getBaseContext().getSystemService(
						Activity.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.layout_popup_window_contact, null);
		popWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT, true);

		popWindow.showAtLocation(emergengyNo, Gravity.CENTER, 0, 0);

		Button agree = (Button) popupView.findViewById(R.id.id_agree);
		Button decline = (Button) popupView.findViewById(R.id.id_disagree);
		agree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.EMERGENCY_NO, emergengyNo
						.getText().toString());
				editor.commit();
				Toast.makeText(getActivity(), "Sucessfully saved",
						Toast.LENGTH_SHORT).show();
				popWindow.dismiss();
			}
		});
		decline.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.remove(AppConstant.EMERGENCY_NO);
				editor.commit();
				emergengyNo.setText("");
				popWindow.dismiss();
			}
		});

	}

}

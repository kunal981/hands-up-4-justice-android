package com.hufj.app.android.tab.protest;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hufj.app.android.R;

public class ProtestFragment extends Fragment {

	public ProtestFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_protest,
				container, false);

		((TextView) rootView.findViewById(R.id.id_text_title))
				.setText(getString(R.string.text_title_protest));

		SpannableString styledString = new SpannableString(
				getString(R.string.title_protest_text));

		// clickable text
		// ClickableSpan clickableSpan = new ClickableSpan() {
		//
		// @Override
		// public void onClick(View widget) {
		// // We display a Toast. You could do anything you want here.
		//
		// Intent email = new Intent(Intent.ACTION_SEND);
		// email.putExtra(Intent.EXTRA_EMAIL,
		// new String[] { "Handsuptheapp@yahoo.com" });
		// // email.putExtra(Intent.EXTRA_CC, new String[]{ to});
		// // email.putExtra(Intent.EXTRA_BCC, new String[]{to});
		// email.putExtra(Intent.EXTRA_SUBJECT, "HandsUp4Free Protest");
		//
		// email.putExtra(Intent.EXTRA_TEXT, "");
		//
		// // need this to prompts email client only
		// email.setType("message/rfc822");
		//
		// getActivity()
		// .startActivity(
		// Intent.createChooser(email,
		// "Choose an Email client :"));
		//
		// }
		// };
		//
		// styledString.setSpan(clickableSpan, 60, 83, 0);
		styledString.setSpan(new ForegroundColorSpan(Color.RED), 60, 83, 0);
		((TextView) rootView.findViewById(R.id.protest_title))
				.setText(styledString);

		return rootView;
	}
}
package com.hufj.app.android.tab.record;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.hufj.app.android.R;
import com.hufj.app.android.camera.CameraRecorderActivity3;
import com.hufj.app.android.util.AppConstant;

public class VideoRecordFragment3 extends Fragment {

	Button buttonRecord, buttonClose;

	SharedPreferences sharedpreferences;

	private PopupWindow popWindow;

	private RelativeLayout relative;

	View rootView;

	public VideoRecordFragment3() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedpreferences = getActivity().getSharedPreferences(
				AppConstant.APP_HFUJ, Context.MODE_PRIVATE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_tab_video_record_,
					container, false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		relative = (RelativeLayout) rootView
				.findViewById(R.id.layout_container_1);
		buttonRecord = (Button) rootView.findViewById(R.id.id_btn_record);

		buttonRecord.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!buttonRecord.isSelected()) {
					initCameraActivity();
				}
			}
		});
		LocationManager locationManager = (LocationManager) getActivity()
				.getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
				|| locationManager
						.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			checkCameraOrientation();
		} else {
			showSettingsAlert();
		}

		return rootView;
	}

	private void checkCameraOrientation() {

		if (sharedpreferences != null
				&& !sharedpreferences.contains(AppConstant.CAMERA_ORIENTATION)) {
			showPopWindow();
		} else {
			startCameraOnStratUp();
		}

	}

	private void startCameraOnStratUp() {
		if (sharedpreferences != null
				&& sharedpreferences.contains(AppConstant.CAMERA_MODE)) {
			if (sharedpreferences.getString(AppConstant.CAMERA_MODE,
					AppConstant.CAMERA_AUTO).equals(AppConstant.CAMERA_AUTO)) {
				initCameraActivity();
			}
		} else {
			initCameraActivity();
		}

	}

	private void initCameraActivity() {

		Intent intent = new Intent(getActivity(), CameraRecorderActivity3.class);
		getActivity().startActivityForResult(intent,
				AppConstant.CAMERA_RQUEST_CODE);
	}

	private void showPopWindow() {

		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getBaseContext().getSystemService(
						Activity.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.layout_popup_window,
				null);
		popWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT, true);

		popWindow.showAtLocation(relative, Gravity.CENTER, 0, 0);
		RadioButton frontCamera = (RadioButton) popupView
				.findViewById(R.id.radio0);
		RadioButton rearComera = (RadioButton) popupView
				.findViewById(R.id.radio1);
		RadioButton cAuto = (RadioButton) popupView
				.findViewById(R.id.radioCameraAuto);
		RadioButton cManual = (RadioButton) popupView
				.findViewById(R.id.radioCameraManual);
		Button submit = (Button) popupView.findViewById(R.id.buttonSubmit);
		rearComera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_ORIENTATION,
						AppConstant.CAMERA_REAR);
				editor.commit();
			}
		});
		frontCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_ORIENTATION,
						AppConstant.CAMERA_FRONT);
				editor.commit();
			}
		});

		cAuto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_MODE,
						AppConstant.CAMERA_AUTO);
				editor.commit();
			}
		});
		cManual.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.CAMERA_MODE,
						AppConstant.CAMERA_MANUAL);
				editor.commit();
			}
		});
		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popWindow.dismiss();
				startCameraOnStratUp();
			}
		});

	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

}

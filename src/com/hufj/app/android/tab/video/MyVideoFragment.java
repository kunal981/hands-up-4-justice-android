package com.hufj.app.android.tab.video;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hufj.app.android.R;
import com.hufj.app.android.dao.ItemVideo;
import com.hufj.app.android.db.AppDatabase;
import com.hufj.app.android.util.AppConstant;

public class MyVideoFragment extends Fragment {

	private ListView listViewVideo;
	private CustomVideoListAdapter cAdapter;
	private List<ItemVideo> listVideos;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_tab_my_video,
				container, false);
		((TextView) rootView.findViewById(R.id.id_text_title))
				.setText(getString(R.string.text_title_myVideo));
		listViewVideo = (ListView) rootView.findViewById(R.id.listVideos);
		new GetVideosTask().execute();

		return rootView;
	}

	private List<ItemVideo> setupVideos() {

		List<ItemVideo> listItemVideo = new ArrayList<>();
		ItemVideo item = new ItemVideo();

		File[] fileList = null;
		File videoFiles;
		try {
			videoFiles = new File(Environment.getExternalStorageDirectory()
					+ "/" + AppConstant.VIDEO_FOLDER);

			if (videoFiles.isDirectory()) {
				fileList = videoFiles.listFiles();
			}

			for (int i = 0; i < fileList.length; i++) {
				Log.e("Video:" + i + " File name", String.valueOf(fileList[i]));
				item = setUpVideoMeta(fileList[i]);
				if (item != null) {
					listItemVideo.add(item);
				}
			}

		} catch (Exception e) {
			Log.e("MyVideoFragment", "Error");
		}
		return listItemVideo;
	}

	private List<ItemVideo> setupVideos2() {

		List<ItemVideo> listItemVideo = new ArrayList<>();
		AppDatabase ap = new AppDatabase(getActivity());

		try {
			listItemVideo = ap.getVideosList();
		} catch (Exception e) {
			// TODO: handle exception
		}

		return listItemVideo;
	}

	@SuppressLint("NewApi")
	private ItemVideo setUpVideoMeta(File file) {
		ItemVideo item = null;
		try {
			if (String.valueOf(file).endsWith(".mp4")) {
				item = new ItemVideo();
				MediaMetadataRetriever retriever = new MediaMetadataRetriever();
				retriever.setDataSource(file.getAbsolutePath());
				// if (Build.VERSION.SDK_INT >= 14)
				// retriever.setDataSource(file.getAbsolutePath(),
				// new HashMap<String, String>());
				// else
				String time = retriever
						.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
				long timeInmillisec = Long.parseLong(time);
				long duration = timeInmillisec / 1000;
				long hours = duration / 3600;
				long minutes = (duration - hours * 3600) / 60;
				long seconds = duration - (hours * 3600 + minutes * 60);
				String videoDuration = String.valueOf(hours + ":" + minutes
						+ ":" + seconds);
				item.setDuration(videoDuration);
				item.setPath(file.getAbsolutePath());
				long millisecond = file.lastModified();
				String dateString = DateFormat.format("MM/dd/yyyy",
						new Date(millisecond)).toString();
				item.setDate(dateString);
				String fileLength = humanReadableByteCount(file.length(), true);
				item.setLength(fileLength);

			}

		} catch (Exception e) {
			item = null;
			// TODO: handle exception
		}
		return item;

	}

	public static String humanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1)
				+ (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	public class CustomVideoListAdapter extends BaseAdapter {

		LayoutInflater inflater;
		public Context context;
		List<ItemVideo> listVideos;

		public CustomVideoListAdapter(Context context,
				List<ItemVideo> listVideos) {
			this.context = context;
			this.listVideos = listVideos;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return listVideos.size();
		}

		public void updateList(List<ItemVideo> listVideos) {
			this.listVideos = listVideos;
			notifyDataSetChanged();
		}

		@Override
		public Object getItem(int position) {
			return listVideos.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.layout_list_video_item_row, null);
				holder = new ViewHolder();
				holder.thumbNail = (ImageView) convertView
						.findViewById(R.id.id_play_view);
				holder.date = (TextView) convertView
						.findViewById(R.id.id_text_date);
				holder.duration = (TextView) convertView
						.findViewById(R.id.id_text_duration);
				holder.address = (TextView) convertView
						.findViewById(R.id.id_text_address);
				holder.length = (TextView) convertView
						.findViewById(R.id.id_text_length);
				holder.buttonDelete = (Button) convertView
						.findViewById(R.id.id_btn_delete);
				holder.buttonShare = (Button) convertView
						.findViewById(R.id.id_btn_share);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final ItemVideo item = listVideos.get(position);

			holder.date.setText(String.valueOf("Date: " + item.getDate()));
			holder.duration.setText(String.valueOf("Duration: "
					+ item.getDuration()));
			holder.address.setText(item.getAddress());
			holder.length.setText(String.valueOf(item.getLength()));

			Bitmap thumb = ThumbnailUtils.createVideoThumbnail(item.getPath(),
					MediaStore.Images.Thumbnails.MINI_KIND);
			holder.thumbNail.setImageBitmap(thumb);
			holder.thumbNail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Uri uri = Uri.parse(item.getPath());
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					intent.setDataAndType(uri, "video/mp4");
					startActivity(intent);

				}
			});
			holder.buttonDelete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showAlertDialog(context, item);

				}

			});
			holder.buttonShare.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					shareVideo(item);

				}

			});

			return convertView;
		}

	}

	static class ViewHolder {

		ImageView thumbNail;
		TextView date;
		TextView duration;
		TextView address;
		TextView length;
		Button buttonDelete;
		Button buttonShare;

	}

	class GetVideosTask extends AsyncTask<Void, Void, List<ItemVideo>> {

		@Override
		protected List<ItemVideo> doInBackground(Void... params) {

			listVideos = setupVideos2();

			listVideos = filterVideos(listVideos);
			return listVideos;
		}

		@Override
		protected void onPostExecute(List<ItemVideo> listVideos) {

			cAdapter = new CustomVideoListAdapter(getActivity(), listVideos);
			listViewVideo.setAdapter(cAdapter);

		}

	}

	protected void shareVideo(ItemVideo item) {
		MediaScannerConnection.scanFile(getActivity(),
				new String[] { item.getPath() }, null,
				new MediaScannerConnection.OnScanCompletedListener() {
					public void onScanCompleted(String path, Uri uri) {
						Log.d("", "onScanCompleted uri " + uri);

						Intent sharingIntent = new Intent(
								android.content.Intent.ACTION_SEND);
						sharingIntent.setType("video/*");
						sharingIntent.putExtra(
								android.content.Intent.EXTRA_SUBJECT,
								"Share youtube");
						sharingIntent.putExtra(
								android.content.Intent.EXTRA_STREAM, uri);
						// sharingIntent.setType("video/*");
						// File newFile = new File(path_var);
						PackageManager pm = getActivity().getPackageManager();
						// Intent openInChooser = Intent.createChooser(
						// sharingIntent, "Where you want to share?");

						List<ResolveInfo> resInfo = pm.queryIntentActivities(
								sharingIntent, 0);
						List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
						for (int i = 0; i < resInfo.size(); i++) {
							ResolveInfo ri = resInfo.get(i);
							String packageName = ri.activityInfo.packageName;
							if (packageName.contains("youtube")) {
								// sharingIntent.setComponent(new ComponentName(
								// packageName, ri.activityInfo.name));
								sharingIntent.setClassName(packageName,
										ri.activityInfo.name);
								// intentList.add(new
								// LabeledIntent(sharingIntent,
								// packageName, ri.loadLabel(pm), ri.icon));
								break;
							}
						}
						getActivity().startActivity(sharingIntent);
					}
				});
		// Intent sharingIntent = new
		// Intent(android.content.Intent.ACTION_SEND);
		// sharingIntent.setType("video/*");
		// sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
		// "Share youtube");
		// sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM,
		// item.getPath());
		// // sharingIntent.setType("video/*");
		// // File newFile = new File(path_var);
		// PackageManager pm = getActivity().getPackageManager();
		// // Intent openInChooser = Intent.createChooser(
		// // sharingIntent, "Where you want to share?");
		//
		// List<ResolveInfo> resInfo = pm.queryIntentActivities(sharingIntent,
		// 0);
		// List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
		// for (int i = 0; i < resInfo.size(); i++) {
		// ResolveInfo ri = resInfo.get(i);
		// String packageName = ri.activityInfo.packageName;
		// if (packageName.contains("youtube")) {
		// sharingIntent.setComponent(new ComponentName(packageName,
		// ri.activityInfo.name));
		// // sharingIntent.setClassName(packageName,
		// // ri.activityInfo.name);
		// // intentList.add(new
		// // LabeledIntent(sharingIntent,
		// // packageName, ri.loadLabel(pm), ri.icon));
		// break;
		// }
		// }
		//
		// getActivity().startActivity(sharingIntent);

	}

	public List<ItemVideo> filterVideos(List<ItemVideo> listVideos) {
		File[] fileList = null;
		File videoFiles;
		List<ItemVideo> listOfVideos = listVideos;
		List<ItemVideo> tempListOfVideos = listVideos;
		try {
			videoFiles = new File(Environment.getExternalStorageDirectory()
					+ "/" + AppConstant.VIDEO_FOLDER);

			if (videoFiles.isDirectory()) {
				fileList = videoFiles.listFiles();
			}

			for (ItemVideo itemVideo : tempListOfVideos) {
				boolean isfileExit = false;
				if (fileList.length != 0) {

					for (int i = 0; i < fileList.length; i++) {
						File file = fileList[i];
						if (itemVideo.getPath().equals(file.getAbsolutePath())) {
							isfileExit = true;
							break;
						}

					}
					if (!isfileExit) {
						listVideos.remove(itemVideo);
					}
				} else {
					listVideos = new ArrayList<>();
				}
			}

		} catch (Exception e) {
			Log.e("MyVideoFragment", "Error: " + e.getMessage());
		}

		return listVideos;

	}

	protected void showAlertDialog(Context context, final ItemVideo item) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle("Delete Video");

		// set dialog message
		alertDialogBuilder
				.setMessage("Are you sure to delete this video ?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								deleteItemFromList(item);
								dialog.cancel();

							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	protected void deleteItemFromList(ItemVideo item) {

		File file = new File(item.getPath());
		boolean deleted = file.delete();
		if (deleted) {
			listVideos.remove(item);
			cAdapter.updateList(listVideos);
			new DeleteVideoTask().execute(item);
		} else {
			Toast.makeText(getActivity(), "Unable to delete file.",
					Toast.LENGTH_SHORT).show();
		}

	}

	class DeleteVideoTask extends AsyncTask<ItemVideo, Void, Void> {

		@Override
		protected Void doInBackground(ItemVideo... params) {
			ItemVideo itemVideo = params[0];
			// TODO Auto-generated method stub
			deleteVideoFromDb(itemVideo);
			return null;
		}
	}

	public void deleteVideoFromDb(ItemVideo itemVideo) {
		AppDatabase ap = new AppDatabase(getActivity());
		ap.deleteVideoByIdCode(itemVideo.getId());
	}

}
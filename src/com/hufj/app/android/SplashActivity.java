package com.hufj.app.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;

import com.hufj.app.android.util.AppConstant;

public class SplashActivity extends Activity {

	private int SPLASH_TIME_OUT = 1200;
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	private SharedPreferences mPref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);
		mPref = getSharedPreferences(AppConstant.APP_HFUJ, Context.MODE_PRIVATE);
		isInternetPresent = isConnectingToInternet();
		checkInternet(isInternetPresent);

	}

	public boolean isConnectingToInternet() {
		ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}

	/**
	 * Function to display simple Alert Dialog
	 * 
	 * @param context
	 *            - application context
	 * @param title
	 *            - alert dialog title
	 * @param message
	 *            - alert message
	 * @param status
	 *            - success/failure (used to set icon)
	 * */
	public void showAlertDialog(Context context, String title, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setIcon(R.drawable.fail);

		// set dialog message
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();
						isInternetPresent = isConnectingToInternet();
						checkInternet(isInternetPresent);

					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
								SplashActivity.this.finish();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	private void checkInternet(Boolean isInternetPresent2) {
		// check for Internet status
		if (isInternetPresent2) {
			new Handler().postDelayed(new Runnable() {
				/*
				 * Showing splash screen with a timer. This will be useful when you
				 * want to show case your app logo / company
				 */

				@Override
				public void run() {
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent intent_home;
					boolean userGuideStatus = (mPref.getBoolean(
							AppConstant.USER_GUIDE, false));
					if (userGuideStatus) {
						intent_home = new Intent(SplashActivity.this,
								MainActivity.class);
					} else {
						intent_home = new Intent(SplashActivity.this,
								UserAgreementActivity.class);
					}
					startActivity(intent_home);
					finish();
				}
			}, SPLASH_TIME_OUT);
		} else {
			// Internet connection is not present
			// Ask user to connect to Internet
			showAlertDialog(SplashActivity.this, "No Internet Connection",
					"You don't have internet connection.");
		}

	}
}

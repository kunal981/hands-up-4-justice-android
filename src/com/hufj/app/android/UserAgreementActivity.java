package com.hufj.app.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.hufj.app.android.util.AppConstant;

public class UserAgreementActivity extends Activity {

	CheckBox radioAgreeButton;
	Button buttonAgree;

	private SharedPreferences mPref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agreement);

		mPref = getSharedPreferences(AppConstant.APP_HFUJ, Context.MODE_PRIVATE);
		radioAgreeButton = (CheckBox) (findViewById(R.id.checkbox_aggree));
		buttonAgree = (Button) (findViewById(R.id.id_button_agree));

		buttonAgree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (radioAgreeButton.isChecked()) {
					Editor editor = mPref.edit();
					editor.putBoolean(AppConstant.USER_GUIDE, true);
					editor.commit();
					Intent intent_home = new Intent(UserAgreementActivity.this,
							MainActivity.class);
					startActivity(intent_home);
					finish();
				} else {
					showAlertDialog(UserAgreementActivity.this, "Agree",
							"Please accept terms and conditions");
				}

			}
		});

		// radioAgreeButton.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// if (!radioAgreeButton.isChecked()) {
		// radioAgreeButton.setChecked(true);
		// } else {
		// radioAgreeButton.setChecked(false);
		// }
		// }
		// });

	}

	public void showAlertDialog(Context context, String title, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setIcon(R.drawable.fail);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}

package com.hufj.app.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.hufj.app.android.container.BaseContainerFragment;
import com.hufj.app.android.container.TabCautionFragmentHoderContainer;
import com.hufj.app.android.container.TabMyVideoFragmentHoderContainer;
import com.hufj.app.android.container.TabProtestFragmentHoderContainer;
import com.hufj.app.android.container.TabVideoRecordFragmentHoderContainer;
import com.hufj.app.android.fragment.AboutUsFragment;
import com.hufj.app.android.fragment.SettingFragment;
import com.hufj.app.android.util.AppConstant;

public class MainActivity extends ActionBarActivity {

	private FragmentTabHost mTabHost;

	private static final String TAB_MY_VIDEO = "VIDEOS";
	private static final String TAB_RECORD = "RECORD";
	private static final String TAB_DO_DONT = "DO/DONT'S";
	private static final String TAB_PROTEST = "PROTEST";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_RECORD),
						R.drawable.tab_item_selector, R.drawable.ic_record),
				TabVideoRecordFragmentHoderContainer.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_MY_VIDEO),
						R.drawable.tab_item_selector, R.drawable.ic_video),
				TabMyVideoFragmentHoderContainer.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_DO_DONT),
						R.drawable.tab_item_selector, R.drawable.ic_caution),
				TabCautionFragmentHoderContainer.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_PROTEST),
						R.drawable.tab_item_selector, R.drawable.ic_protest),
				TabProtestFragmentHoderContainer.class, null);

	}

	public TabSpec setIndicator(TabSpec spec, int tabId, int iconId) {
		View v = LayoutInflater.from(this).inflate(R.layout.tab_item, null);
		v.setBackgroundResource(tabId);
		ImageView imageButton = (ImageView) v
				.findViewById(R.id.id_icon_tab_image);
		imageButton.setImageResource(iconId);
		TextView text = (TextView) v.findViewById(R.id.id_text_tabs);
		text.setText(spec.getTag());

		return spec.setIndicator(v);
	}

	@Override
	public void onBackPressed() {
		boolean isPopFragment = false;
		String currentTabTag = mTabHost.getCurrentTabTag();

		if (currentTabTag.equals(TAB_MY_VIDEO)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_MY_VIDEO)).popFragment();
		} else if (currentTabTag.equals(TAB_RECORD)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_RECORD)).popFragment();
		} else if (currentTabTag.equals(TAB_DO_DONT)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_DO_DONT)).popFragment();
		} else if (currentTabTag.equals(TAB_PROTEST)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_PROTEST)).popFragment();
		}

		if (!isPopFragment) {

			// if (mTabHost.getCurrentTab() == 0) {
			// finish();
			// } else {
			// mTabHost.setCurrentTab(0);
			// }

			finish();
		}
	}

	public void onSettingButtonClick(View view) {
		String currentTabTag = mTabHost.getCurrentTabTag();
		SettingFragment fragment = new SettingFragment();
		((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(
				currentTabTag)).addSettingFragment(fragment, true);

	}

	public void onAboutButtonClick(View view) {
		String currentTabTag = mTabHost.getCurrentTabTag();
		AboutUsFragment fragment = new AboutUsFragment();
		((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(
				currentTabTag)).addSettingFragment(fragment, true);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == AppConstant.CAMERA_RQUEST_CODE) {

			if (resultCode == RESULT_OK) {
				mTabHost.setCurrentTab(1);
			}
		}
	}

}

package com.hufj.app.android.db;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.hufj.app.android.dao.ItemVideo;

public class AppDatabase {

	public static final String TAG = AppDatabase.class.getSimpleName();

	private DBHelper opener;
	private SQLiteDatabase db;
	Context context;

	public AppDatabase(Context context) {
		this.context = context;
		this.opener = new DBHelper(context);
		db = opener.getWritableDatabase();
	}

	private SQLiteDatabase getDb() {
		return opener.getWritableDatabase();
	}

	class DBHelper extends SQLiteOpenHelper {
		public static final int version = 2;

		public DBHelper(Context context) {
			super(context, "husf.db", null, DBHelper.version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.w(TAG, "App Database onCreate");
			db.execSQL(this.stringFromAssets("sql/video.ddl"));

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Database Version : Old Version:" + oldVersion
					+ "  New Version:" + newVersion);
			String sql = "DROP TABLE IF EXISTS Video";
			db.execSQL(sql);
			onCreate(db);

		}

		public String stringFromAssets(String fileName) {
			StringBuilder returnString = new StringBuilder();
			InputStream fIn = null;
			InputStreamReader isr = null;
			BufferedReader input = null;
			try {
				fIn = context.getResources().getAssets()
						.open(fileName, Context.MODE_PRIVATE);
				isr = new InputStreamReader(fIn);
				input = new BufferedReader(isr);
				String line = "";
				while ((line = input.readLine()) != null) {
					returnString.append(line);
				}
			} catch (Exception e) {
				e.getMessage();
			} finally {
				try {
					if (isr != null)
						isr.close();
					if (fIn != null)
						fIn.close();
					if (input != null)
						input.close();
				} catch (Exception e2) {
					e2.getMessage();
				}
			}
			return returnString.toString();
		}

	}

	// Using db method
	// It's global setting for all books
	public long insertSetting(ItemVideo itemVideo) {
		long id = -1;
		db = getDb();
		try {
			ContentValues values = new ContentValues();
			values.put("Title", itemVideo.getTitle());
			values.put("Date", itemVideo.getDate());
			values.put("Duration", itemVideo.getDuration());
			values.put("Length", itemVideo.getLength());
			values.put("Latitude", itemVideo.getLatitude());
			values.put("Longitude", itemVideo.getLongitude());
			values.put("Path", itemVideo.getPath());
			values.put("Address", itemVideo.getAddress());
			id = db.insert("Video", null, values);
		} catch (Exception e) {
			Log.e(TAG, "Error while inserting record" + e.getLocalizedMessage());
		} finally {
			db.close();
		}

		return id;
	}

	public List<ItemVideo> getVideosList() {
		String sql = "SELECT * FROM Video where 1=1";
		db = getDb();
		Cursor result = db.rawQuery(sql, null);
		List<ItemVideo> listVideos = new ArrayList<>();
		try {
			result.moveToNext();
			while (!result.isAfterLast()) {
				ItemVideo video = new ItemVideo();
				video.setId(result.getInt(0));
				video.setTitle(result.getString(1));
				video.setDate(result.getString(2));
				video.setDuration(result.getString(3));
				video.setLength(result.getString(4));
				video.setLatitude(result.getString(5));
				video.setLongitude(result.getString(6));
				video.setPath(result.getString(7));
				video.setAddress(result.getString(8));
				listVideos.add(video);
				result.moveToNext();

			}

			result.close();
		} catch (Exception e) {
			Log.e(TAG, e.getLocalizedMessage());
		} finally {
			db.close();
		}
		return listVideos;
	}

	public void deleteVideoByIdCode(int id) {
		db = getDb();
		try {
			String sql = String.format(Locale.US,
					"DELETE FROM Video where VideoId = %d", id);
			db.execSQL(sql);

		} catch (Exception e) {
			Log.e(TAG, e.getLocalizedMessage());
		} finally {
			db.close();
		}
	}
}

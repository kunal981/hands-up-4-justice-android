package com.hufj.app.android.tab.caution;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hufj.app.android.R;

public class CautionFragment extends Fragment {

	public CautionFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_caution,
				container, false);
		
		((TextView) rootView.findViewById(R.id.id_text_title))
		.setText(getString(R.string.text_title_do_donts));

		return rootView;
	}
}
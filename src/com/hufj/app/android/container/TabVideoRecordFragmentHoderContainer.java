package com.hufj.app.android.container;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hufj.app.android.R;
import com.hufj.app.android.tab.record.VideoRecordFragment3;

public class TabVideoRecordFragmentHoderContainer extends BaseContainerFragment {

	private boolean mIsViewInited;
	private static final String TAG = TabVideoRecordFragmentHoderContainer.class
			.getName();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "oncreateview");
		return inflater.inflate(R.layout.container_framelayout, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i(TAG, "container on activity created");
		if (!mIsViewInited) {
			mIsViewInited = true;
			initView();
		} else {
			getChildFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}

	private void initView() {
		Log.i(TAG, "tab 1 init view");
		replaceFragment(new VideoRecordFragment3(), false);
	}

}

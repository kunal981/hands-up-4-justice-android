package com.hufj.app.android.util;

public class AppConstant {

	public static final String VIDEO_FOLDER = "HandsUpForFree";
	public static final String APP_HFUJ = "hfuj";

	public static final String CAMERA_ORIENTATION = "cOrientation";
	public static final String CAMERA_MODE = "cMode";
	public static final String CAMERA_REAR = "cRear";
	public static final String CAMERA_FRONT = "cFront";
	public static final String CAMERA_AUTO = "cAuto";
	public static final String CAMERA_MANUAL = "cManual";
	public static final String USER_GUIDE = "uGuide";

	public static final String EMERGENCY_NO = "pEmergency";

	// request code for Camera Open
	public static final int CAMERA_RQUEST_CODE = 328;

}

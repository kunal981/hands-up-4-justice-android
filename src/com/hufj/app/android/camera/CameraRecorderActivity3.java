package com.hufj.app.android.camera;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.AllClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.CamcorderProfile;
import android.media.MediaMetadataRetriever;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.telephony.SmsManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.hufj.app.android.R;
import com.hufj.app.android.dao.ItemVideo;
import com.hufj.app.android.db.AppDatabase;
import com.hufj.app.android.util.AppConstant;
import com.hufj.app.android.util.LocationUtils;

public class CameraRecorderActivity3 extends FragmentActivity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	public static final String TAG = CameraRecorderActivity3.class
			.getSimpleName();

	private int SPLASH_TIME_OUT = 10000;

	private boolean isRecording = false;

	Button buttonStop;
	private MediaRecorder mMediaRecorder;
	private Camera mCamera;
	CameraPreview mPreview;

	RelativeLayout relativeOuterTrans, relativeOuter;

	ProgressDialog pDialog;
	Timer timer;

	SharedPreferences sharedpreferences;
	String cameraPref;
	String number;

	SentBroadcast sentBroadcast;
	DeliveredBroadcast deliveredBroadcast;

	// location finder
	private boolean isLocationManager = false;
	// A request to connect to Location Services
	private LocationRequest mLocationRequest;
	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	Location currentLocation;
	double latitude;
	double longitude;
	String currentaddress;
	String pathToVideo;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera2);

		sharedpreferences = getSharedPreferences(AppConstant.APP_HFUJ,
				Context.MODE_PRIVATE);
		cameraPref = sharedpreferences.getString(
				AppConstant.CAMERA_ORIENTATION, AppConstant.CAMERA_REAR);
		number = sharedpreferences.getString(AppConstant.EMERGENCY_NO, "");
		// Create an instance of Camera
		mCamera = getCameraInstance();

		// Create our Preview view and set it as the content of our
		// activity.
		mPreview = new CameraPreview(this, mCamera);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(mPreview);

		relativeOuterTrans = (RelativeLayout) findViewById(R.id.id_relative_outer_trans);

	}

	@Override
	protected void onStart() {
		super.onStart();
		locationInitilizer();
	}

	public void setupUi() {

		relativeOuterTrans = (RelativeLayout) findViewById(R.id.id_relative_outer_trans);
		relativeOuter = (RelativeLayout) findViewById(R.id.id_relative_outer);
		buttonStop = (Button) findViewById(R.id.stop);
		addbuttonListener();
		startVideoRecording();
		relativeOuterTrans.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				toogleVisibilty();
				return false;
			}
		});

	}

	public void hideSurFace() {
		new Handler().postDelayed(new Runnable() {

			/*
			* Showing splash screen with a timer. This will be useful when you
			* want to show case your app logo / company
			*/

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				relativeOuter.setVisibility(View.INVISIBLE);
				buttonStop.setVisibility(View.INVISIBLE);
			}
		}, SPLASH_TIME_OUT);
	}

	public void toogleVisibilty() {
		if (relativeOuter.getVisibility() == View.INVISIBLE) {
			relativeOuter.setVisibility(View.VISIBLE);
			buttonStop.setVisibility(View.VISIBLE);
		} else if (relativeOuter.getVisibility() == View.VISIBLE) {
			relativeOuter.setVisibility(View.INVISIBLE);
			buttonStop.setVisibility(View.INVISIBLE);
		}
	}

	private void addbuttonListener() {
		buttonStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isRecording) {
					stopVideoRecording();
					saveVideoToDatabase();
					timer.cancel();
					sendConfirmMessage();
				} else {
					if (prepareVideoRecorder()) {
						// Camera is available and unlocked, MediaRecorder is
						// prepared,
						// now you can start recording
						buttonStop.setSelected(true);
						mMediaRecorder.start();

						// inform the user that recording has started
						isRecording = true;
					} else {
						// prepare didn't work, release the camera
						releaseMediaRecorder();
						// inform user
					}
				}

			}
		});
	}

	protected void sendConfirmMessage() {
		if (!number.equals("")) {
			sendSMS(number,
					"The Hands up app has recorded a video and sent it to Dropbox.");
		} else {
			Toast.makeText(this,
					"Please set your emergency number on setting page",
					Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void onBackPressed() {
		if (isRecording) {
			toogleVisibilty();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stopVideoRecording();
			saveVideoToDatabase();
			timer.cancel();
			sendConfirmMessage();
		} else {
			finish();
		}

	}

	protected void saveVideoToDatabase() {
		new SaveVideoInfoTask().execute();
	}

	public void startVideoRecording() {
		if (prepareVideoRecorder()) {
			// Camera is available and unlocked, MediaRecorder is
			// prepared,
			// now you can start recording
			buttonStop.setSelected(true);
			mMediaRecorder.start();

			// inform the user that recording has started
			isRecording = true;
		} else {
			// prepare didn't work, release the camera
			releaseMediaRecorder();
			// inform user
		}
	}

	public void stopVideoRecording() {
		buttonStop.setSelected(false);
		if (mMediaRecorder != null)
			mMediaRecorder.stop(); // stop the recording
		releaseMediaRecorder();
		// release the MediaRecorder object
		if (mCamera != null)
			mCamera.lock();
		isRecording = false;
	}

	private void setUpTimerForAutoRecording() {
		final Handler handler = new Handler();
		timer = new Timer();
		TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						try {
							// "Your function call  "
							Log.e(TAG, "RestartRecording");
							stopVideoRecording();
							new SaveVideoInbetweenInfoTask().execute();

						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
					}
				});
			}
		};
		timer.schedule(doAsynchronousTask, 120000, 120000);

	}

	public class CameraPreview extends SurfaceView implements
			SurfaceHolder.Callback {
		private SurfaceHolder mHolder;
		private Camera mCamera;

		public CameraPreview(Context context, Camera camera) {
			super(context);
			mCamera = camera;

			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = getHolder();
			mHolder.addCallback(this);
			// deprecated setting, but required on Android versions prior to 3.0
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void surfaceCreated(SurfaceHolder holder) {
			// The Surface has been created, now tell the camera where to draw
			// the preview.
			try {
				mCamera.setPreviewDisplay(holder);
				mCamera.startPreview();
			} catch (IOException e) {
				Log.d(TAG, "Error setting camera preview: " + e.getMessage());
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// empty. Take care of releasing the Camera preview in your
			// activity.
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w,
				int h) {
			// If your preview can change or rotate, take care of those events
			// here.
			// Make sure to stop the preview before resizing or reformatting it.

			if (mHolder.getSurface() == null) {
				// preview surface does not exist
				return;
			}

			// stop preview before making changes
			try {
				mCamera.stopPreview();
			} catch (Exception e) {
				// ignore: tried to stop a non-existent preview
			}

			// set preview size and make any resize, rotate or
			// reformatting changes here

			// start preview with new settings
			try {
				mCamera.setPreviewDisplay(mHolder);
				mCamera.startPreview();

			} catch (Exception e) {
				Log.d(TAG, "Error starting camera preview: " + e.getMessage());
			}
		}
	}

	public Camera getCameraInstance() {
		Camera mServiceCamera = null;
		try {
			if (cameraPref.equals(AppConstant.CAMERA_FRONT)) {
				mServiceCamera = openFrontFacingCameraGingerbread();
			} else {
				mServiceCamera = Camera.open(0);
			}
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
			Log.e(TAG, "Camera not abailable");
		}

		if (mServiceCamera == null) {
			Log.e(TAG, "Camera not abailable");
			Toast.makeText(this, "Camera initialization failed",
					Toast.LENGTH_SHORT).show();
			finish();
		}
		return mServiceCamera; // returns null if camera is unavailable
	}

	private Camera openFrontFacingCameraGingerbread() {
		int cameraCount = 0;
		Camera cam = null;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		cameraCount = Camera.getNumberOfCameras();
		for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
			Camera.getCameraInfo(camIdx, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				try {
					cam = Camera.open(camIdx);
				} catch (RuntimeException e) {
					Log.e("CustomCameraPreview",
							"Camera failed to open: " + e.getLocalizedMessage());
				}
			}
		}

		return cam;
	}

	private boolean prepareVideoRecorder() {

		if (mCamera == null)
			mCamera = getCameraInstance();
		mMediaRecorder = new MediaRecorder();

		// Step 1: Unlock and set camera to MediaRecorder
		mCamera.unlock();
		mMediaRecorder.setCamera(mCamera);

		// Step 2: Set sources
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

		// Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
		// mMediaRecorder.setProfile(CamcorderProfile
		// .get(CamcorderProfile.QUALITY_HIGH));
		mMediaRecorder.setProfile(CamcorderProfile.get(1,
				CamcorderProfile.QUALITY_HIGH));
		// mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		// mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		// mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
		// mMediaRecorder.setVideoFrameRate(30);
		// mMediaRecorder.setVideoEncodingBitRate(500);
		// mMediaRecorder.setAudioEncodingBitRate(128);

		// Step 4: Set output file
		mMediaRecorder.setOutputFile(getOutputMediaFile());

		// Step 5: Set the preview output
		mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

		// Step 6: Prepare configured MediaRecorder
		try {
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.d(TAG,
					"IllegalStateException preparing MediaRecorder: "
							+ e.getMessage());
			releaseMediaRecorder();
			return false;
		} catch (IOException e) {
			Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		}
		return true;
	}

	private void releaseMediaRecorder() {
		if (mMediaRecorder != null) {
			mMediaRecorder.reset(); // clear recorder configuration
			mMediaRecorder.release(); // release the recorder object
			mMediaRecorder = null;
			mCamera.lock(); // lock camera for later use
		}
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	@Override
	protected void onResume() {
		Log.e(TAG, "OnPause");
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();

		Log.e(TAG, "OnPause");
		releaseMediaRecorder(); // if you are using MediaRecorder, release it
		releaseCamera(); // release the camera immediately on pause event
		if (isRecording) {
			timer.cancel();
			new SaveVideoUnfortunelyInfoTask().execute();
		}

		if (sentBroadcast != null)
			unregisterReceiver(sentBroadcast);
		if (deliveredBroadcast != null)
			unregisterReceiver(deliveredBroadcast);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.e(TAG, "onDestroy");
		super.onDestroy();

	}

	/** Create a File for saving an image or video */
	private String getOutputMediaFile() {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		String pathToDcim = Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DCIM).getAbsolutePath();
		File mediaStorageDir = new File(
				Environment.getExternalStorageDirectory(),
				AppConstant.VIDEO_FOLDER);
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("MyCameraApp", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;

		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "VID_" + timeStamp + ".mp4");

		pathToVideo = mediaFile.toString();
		return pathToVideo;
	}

	// saving video info to database

	private long saveVideo() {
		long id = -1;
		ItemVideo itemVideo;
		AppDatabase appDb = new AppDatabase(this);
		itemVideo = setUpVideoMeta(new File(pathToVideo));
		if (itemVideo != null) {
			id = appDb.insertSetting(itemVideo);
		}
		return id;

	}

	public void updateToList() {
		Intent intent = new Intent();
		intent.putExtra("MESSAGE", "Recorded Successfully");
		setResult(RESULT_OK, intent);
		finish();

	}

	private ItemVideo setUpVideoMeta(File file) {
		ItemVideo item = null;
		try {
			if (String.valueOf(file).endsWith(".mp4")) {
				item = new ItemVideo();
				item.setTitle(file.getName());
				item.setPath(file.getAbsolutePath());
				item.setLatitude(String.valueOf(latitude));
				item.setLongitude(String.valueOf(longitude));
				item.setAddress(currentaddress);
				MediaMetadataRetriever retriever = new MediaMetadataRetriever();
				retriever.setDataSource(file.getAbsolutePath());
				// if (Build.VERSION.SDK_INT >= 14)
				// retriever.setDataSource(file.getAbsolutePath(),
				// new HashMap<String, String>());
				// else
				String time = retriever
						.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

				DecimalFormat formatter = new DecimalFormat("00");

				long timeInmillisec = Long.parseLong(time);
				long duration = timeInmillisec / 1000;
				long hours = duration / 3600;
				long minutes = (duration - hours * 3600) / 60;
				long seconds = duration - (hours * 3600 + minutes * 60);
				String videoDuration = String.valueOf(formatter.format(hours)
						+ ":" + formatter.format(minutes) + ":"
						+ formatter.format(seconds));
				item.setDuration(videoDuration);
				item.setPath(file.getAbsolutePath());
				long millisecond = file.lastModified();
				String dateString = DateFormat.format("dd/MM/yyyy",
						new Date(millisecond)).toString();
				item.setDate(dateString);
				String fileLength = humanReadableByteCount(file.length(), true);
				item.setLength(fileLength);

			}

		} catch (Exception e) {
			item = null;
			// TODO: handle exception
		}
		return item;

	}

	public static String humanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1)
				+ (si ? "" : "i");
		return String.format(Locale.getDefault(), "%.1f %sB",
				bytes / Math.pow(unit, exp), pre);
	}

	public class SaveVideoInfoTask extends AsyncTask<Void, Void, Long> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(CameraRecorderActivity3.this);
			pDialog.show();
		}

		@Override
		protected Long doInBackground(Void... params) {
			return saveVideo();
		}

		@Override
		protected void onPostExecute(Long result) {
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			if (result != -1) {
				// setResult();
				Toast.makeText(CameraRecorderActivity3.this,
						"Video Save Successfully", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(CameraRecorderActivity3.this,
						"Failed to save video", Toast.LENGTH_SHORT).show();
			}
			updateToList();

		}

	}

	public class SaveVideoInbetweenInfoTask extends AsyncTask<Void, Void, Long> {

		@Override
		protected Long doInBackground(Void... params) {
			return saveVideo();
		}

		@Override
		protected void onPostExecute(Long result) {
			if (result != -1) {
				// setResult();
				startVideoRecording();
				Toast.makeText(CameraRecorderActivity3.this,
						"Video info updated", Toast.LENGTH_SHORT).show();
			} else {
				isRecording = false;
				buttonStop.setSelected(false);
				Toast.makeText(CameraRecorderActivity3.this,
						"Failed to save video info", Toast.LENGTH_SHORT).show();
			}

		}

	}

	public class SaveVideoUnfortunelyInfoTask extends
			AsyncTask<Void, Void, Long> {

		@Override
		protected Long doInBackground(Void... params) {
			return saveVideo();
		}

		@Override
		protected void onPostExecute(Long result) {
			finish();
		}

	}

	// Google play services for detecting location demo

	private void locationInitilizer() {

		if (servicesConnected()) {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
					|| locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				mLocationRequest = LocationRequest.create();
				mLocationRequest
						.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
				mLocationRequest.setInterval(1000);
				mLocationRequest.setFastestInterval(500);
				mLocationClient = new LocationClient(this, this, this);
				mLocationClient.connect();
				isLocationManager = true;
			} else {
				showSettingsAlert();
			}

		}

	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d(LocationUtils.APPTAG, "Play service available");

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getSupportFragmentManager(),
						LocationUtils.APPTAG);
			}
			return false;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.e("", "Play Service not Available ");
		finish();

	}

	@Override
	public void onConnected(Bundle arg0) {

		currentLocation = getLocation();

		if (currentLocation != null) {
			// setupUi();
			Toast.makeText(
					this,
					"Latitude: " + currentLocation.getLatitude()
							+ "\nLongitude: " + currentLocation.getLongitude(),
					Toast.LENGTH_SHORT).show();

			latitude = currentLocation.getLatitude();
			longitude = currentLocation.getLongitude();
			Log.e("Location", "Lat: " + latitude + " Longitude:" + longitude);

			new GetAddressTask(this).execute(currentLocation);
		} else {
			Toast.makeText(this, "Unable to find User location ",
					Toast.LENGTH_SHORT).show();
			finish();
		}
		setupUi();
		setUpTimerForAutoRecording();
		hideSurFace();

	}

	@Override
	public void onDisconnected() {

		Log.e("", "On disconnected");

	}

	public Location getLocation() {
		Location currentLocation = null;
		// If Google Play Services is available
		// Get the current location
		if (mLocationClient.isConnected()) {
			currentLocation = mLocationClient.getLastLocation();
		} else {
			mLocationClient.connect();
		}

		// Display the current location in the UI
		// mLatLng.setText(LocationUtils.getLatLng(this, currentLocation));

		return currentLocation;
	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	public static List<Address> getFromLocation(double lat, double lng,
			int maxResult) {

		String address = String
				.format(Locale.ENGLISH,
						"http://maps.googleapis.com/maps/api/geocode/json?latlng=%1$f,%2$f&sensor=false&language="
								+ Locale.getDefault().getCountry(), lat, lng);
		HttpGet httpGet = new HttpGet(address);
		HttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(AllClientPNames.USER_AGENT,
				"Mozilla/5.0 (Java) Gecko/20081007 java-geocoder");
		client.getParams().setIntParameter(AllClientPNames.CONNECTION_TIMEOUT,
				25 * 1000);
		client.getParams().setIntParameter(AllClientPNames.SO_TIMEOUT,
				25 * 1000);
		HttpResponse response;

		List<Address> retList = null;

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			String json = EntityUtils.toString(entity, "UTF-8");

			JSONObject jsonObject = new JSONObject(json);

			retList = new ArrayList<Address>();

			if ("OK".equalsIgnoreCase(jsonObject.getString("status"))) {
				JSONArray results = jsonObject.getJSONArray("results");
				if (results.length() > 0) {
					for (int i = 0; i < results.length() && i < maxResult; i++) {
						JSONObject result = results.getJSONObject(i);
						// Log.e(MyGeocoder.class.getName(), result.toString());
						Address addr = new Address(Locale.getDefault());
						// addr.setAddressLine(0,
						// result.getString("formatted_address"));

						JSONArray components = result
								.getJSONArray("address_components");
						String streetNumber = "";
						String country = "";
						for (int a = 0; a < components.length(); a++) {
							JSONObject component = components.getJSONObject(a);
							JSONArray types = component.getJSONArray("types");
							for (int j = 0; j < types.length(); j++) {
								String type = types.getString(j);
								if (type.equals("locality")) {
									addr.setLocality(component
											.getString("long_name"));
								} else if (type.equals("sublocality_level_1")) {
									streetNumber = component
											.getString("long_name");
								} else if (type.equals("country")) {
									country = component.getString("long_name");
									addr.setCountryName(country);
								}
							}
						}
						addr.setAddressLine(0, streetNumber);

						addr.setLatitude(result.getJSONObject("geometry")
								.getJSONObject("location").getDouble("lat"));
						addr.setLongitude(result.getJSONObject("geometry")
								.getJSONObject("location").getDouble("lng"));
						retList.add(addr);
					}
				}
			}

		} catch (ClientProtocolException e) {
			Log.e("", "Error calling Google geocode webservice.", e);
		} catch (IOException e) {
			Log.e("", "Error calling Google geocode webservice.", e);
		} catch (JSONException e) {
			Log.e("", "Error parsing Google geocode webservice response.", e);
		}

		return retList;
	}

	// all asyncTask

	private class GetAddressTask extends AsyncTask<Location, Void, String> {
		Context mContext;

		public GetAddressTask(Context context) {
			super();
			mContext = context;
		}

		/*
		 * When the task finishes, onPostExecute() displays the address. 
		 */
		@Override
		protected void onPostExecute(String address) {
			// Display the current address in the UI
			currentaddress = address;
			Toast.makeText(CameraRecorderActivity3.this,
					"Address: " + currentaddress, Toast.LENGTH_SHORT).show();

		}

		@Override
		protected String doInBackground(Location... params) {
			String geoAddress;
			List<Address> addresses = null;
			Location loc = params[0];
			if (Geocoder.isPresent()) {
				Log.e("GeoCoder", "GeoCoder available");
				Geocoder geocoder = new Geocoder(CameraRecorderActivity3.this,
						Locale.ENGLISH);

				// Get the current location from the input parameter list
				// Create a list to contain the result address

				try {
					addresses = geocoder.getFromLocation(loc.getLatitude(),
							loc.getLongitude(), 1);
					// addresses = geocoder.getFromLocation(28.6100, 77.2300,
					// 1);
				} catch (IOException e1) {
					Log.e("LocationSampleActivity",
							"IO Exception in getFromLocation()");
					e1.printStackTrace();
				} catch (IllegalArgumentException e2) {
					// Error message to post in the log
					String errorString = "Illegal arguments "
							+ Double.toString(loc.getLatitude()) + " , "
							+ Double.toString(loc.getLongitude())
							+ " passed to address service";
					Log.e("LocationSampleActivity", errorString);
					e2.printStackTrace();
				}
				// If the reverse geocode returned an address

			} else {
				Log.e("GeoCoder", "GeoCoder not available");
				try {
					addresses = getFromLocation(loc.getLatitude(),
							loc.getLongitude(), 1);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if (addresses != null && addresses.size() > 0) {
				// Get the first address
				Address address = addresses.get(0);
				/*
				* Format the first line of address (if available),
				* city, and country name.
				*/
				String addressText = String.format(
						"%s, %s, %s",
						// If there's a street address, add it
						address.getMaxAddressLineIndex() > 0 ? address
								.getAddressLine(0) : "",
						// Locality is usually a city
						address.getLocality(),
						// The country of the address
						address.getCountryName());
				// String addressText = String.format("%s, %s",
				// // If there's a street address, add it
				// // Locality is usually a city
				// address.getLocality(),
				// // The country of the address
				// address.getCountryName());
				// Return the text
				geoAddress = addressText;
			} else {
				geoAddress = "No address found";
			}
			return geoAddress;

		}
	}

	// sending message functionality here
	private void sendSMS(String phoneNumber, String message) {
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";
		sentBroadcast = new SentBroadcast();
		deliveredBroadcast = new DeliveredBroadcast();

		PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
				SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
				new Intent(DELIVERED), 0);
		// ---when the SMS has been sent---
		registerReceiver(sentBroadcast, new IntentFilter(SENT));

		// ---when the SMS has been delivered---
		registerReceiver(deliveredBroadcast, new IntentFilter(DELIVERED));

		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
	}

	class SentBroadcast extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			switch (getResultCode()) {
			case Activity.RESULT_OK:
				Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT)
						.show();
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				Toast.makeText(getBaseContext(), "Generic failure",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_NO_SERVICE:
				Toast.makeText(getBaseContext(), "No service",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_NULL_PDU:
				Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
						.show();
				break;
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				Toast.makeText(getBaseContext(), "Radio off",
						Toast.LENGTH_SHORT).show();
				break;
			}
		}

	}

	class DeliveredBroadcast extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			switch (getResultCode()) {
			case Activity.RESULT_OK:
				Toast.makeText(getBaseContext(), "SMS delivered",
						Toast.LENGTH_SHORT).show();
				break;
			case Activity.RESULT_CANCELED:
				Toast.makeText(getBaseContext(), "SMS not delivered",
						Toast.LENGTH_SHORT).show();
				break;
			}
		}

	}

}